FROM node:12.9.1-alpine

RUN apk update && apk upgrade && \
  apk add --no-cache bash git openssh

WORKDIR /app/application

COPY . /app/application

RUN npm install

RUN npm run build

CMD npm start
