import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Mcapp } from '../models';
import { McappRepository } from '../repositories';

export class McappController {
  constructor(
    @repository(McappRepository)
    public mcappRepository: McappRepository,
  ) { }

  @post('/mcapps', {
    responses: {
      '200': {
        description: 'Mcapp model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Mcapp } } },
      },
    },
  })
  async create(@requestBody() mcapp: Mcapp): Promise<Mcapp> {
    return await this.mcappRepository.create(mcapp);
  }

  @post('/applyMc', {
    responses: {
      '200': {
        description: 'Mcapp model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Mcapp } } },
      },
    },
  })
  async applyMc(@requestBody() mcapp: Mcapp): Promise<Mcapp> {
    return await this.mcappRepository.create(mcapp);
  }



  @post('/upddelete', {
    responses: {
      '200': {
        description: 'Mcapp model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Mcapp } } },
      },
    },
  })
  async upddelete(@requestBody() mcapp: Mcapp): Promise<Mcapp> {
    return await this.mcappRepository.create(mcapp);
  }

  @get('/getAppliedMcsForUser{id}', {
    responses: {
      '200': {
        description: 'Mcapp model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async countgetAppliedMcsForUser(
    @param.query.object('where', getWhereSchemaFor(Mcapp)) where?: Where<Mcapp>,
  ): Promise<Count> {
    return await this.mcappRepository.count(where);
  }

  @get('/getAppliedUsersForMc', {
    responses: {
      '200': {
        description: 'Mcapp model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async getAppliedUsersForMc(
    @param.query.object('where', getWhereSchemaFor(Mcapp)) where?: Where<Mcapp>,
  ): Promise<Count> {
    // where = typeof where === 'string' ? JSON.parse(where) : where;

    return await this.mcappRepository.count(where);
  }

  @get('/mcapps/count', {
    responses: {
      '200': {
        description: 'Mcapp model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Mcapp)) where?: Where<Mcapp>,
  ): Promise<Count> {
    return await this.mcappRepository.count(where);
  }

  @get('/mcapps', {
    responses: {
      '200': {
        description: 'Array of Mcapp model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Mcapp } },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Mcapp)) filter?: Filter<Mcapp>,
  ): Promise<Mcapp[]> {
    return await this.mcappRepository.find(filter);
  }



  @patch('/mcapps', {
    responses: {
      '200': {
        description: 'Mcapp PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Mcapp, { partial: true }),
        },
      },
    })
    mcapp: Mcapp,
    @param.query.object('where', getWhereSchemaFor(Mcapp)) where?: Where<Mcapp>,
  ): Promise<Count> {
    return await this.mcappRepository.updateAll(mcapp, where);
  }

  @get('/mcapps/{id}', {
    responses: {
      '200': {
        description: 'Mcapp model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Mcapp } } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Mcapp> {
    return await this.mcappRepository.findById(id);
  }

  @patch('/mcapps/{id}', {
    responses: {
      '204': {
        description: 'Mcapp PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Mcapp, { partial: true }),
        },
      },
    })
    mcapp: Mcapp,
  ): Promise<void> {
    await this.mcappRepository.updateById(id, mcapp);
  }

  @put('/mcapps/{id}', {
    responses: {
      '204': {
        description: 'Mcapp PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: string,
    @requestBody() mcapp: Mcapp,
  ): Promise<void> {
    await this.mcappRepository.replaceById(id, mcapp);
  }

  @del('/mcapps/{id}', {
    responses: {
      '204': {
        description: 'Mcapp DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: string): Promise<void> {
    await this.mcappRepository.deleteById(id);
  }
}
