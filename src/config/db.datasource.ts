export default {
  "name": process.env.DATASOURCE_NAME || "db",
  "connector": process.env.DATASOURCE_CONNECTOR || "mysql",
  "url": process.env.DATASOURCE_URL || "",
  "host": process.env.DB_HOST || "localhost",
  "port": process.env.DB_PORT || "33066",
  "user": process.env.DB_USER || "root",
  "password": process.env.DB_PASSWORD || "1234",
  "database": process.env.DB_NAME || "application"
}
