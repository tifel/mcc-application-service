import { Entity, model, property } from '@loopback/repository';
import { v4 as uuid } from 'uuid';

@model({ settings: {} })
export class Mcapp extends Entity {
  @property({
    type: 'string',
    id: true,
    default: () => uuid(),
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  userId: string;

  @property({
    type: 'string',
    required: true,
  })
  mcId: string;

  @property({
    type: 'date',
    default: Date
  })
  appliedAt?: string;

  @property({
    type: 'boolean',
    default: true
  })
  status?: boolean;

  @property({
    type: 'string',
  })
  evidenceSubmissionId: 'string'

  @property({
    type: 'date',
    default: Date
  })
  allUpdatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Mcapp>) {
    super(data);
  }
}

export interface McappRelations {
  // describe navigational properties here
}

export type McappWithRelations = Mcapp & McappRelations;
