import {DefaultCrudRepository} from '@loopback/repository';
import {Mcapp, McappRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class McappRepository extends DefaultCrudRepository<
  Mcapp,
  typeof Mcapp.prototype.ID,
  McappRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Mcapp, dataSource);
  }
}
